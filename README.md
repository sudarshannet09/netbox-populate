# Introduction
This playbook populates Netbox using a combination of static information specified in a file and dynamic information extracted from devices using `napalm_get_facts`.

The playbook consists of two plays:
```
Name: Update Base Information 
      Uses a static file as source of information to populate the following 
      in netbox.
       - Sites
       - Regions
Name: Populate Netbox as Source of Truth
      Uses Napalm to extract facts from devices and populates the following 
      in netbox. It also uses some static information from the Inventory file.
       - Manufacturer
       - Device Types
       - Device roles
       - Platform
       - Devices
       - Inventory Items
       - Interfaces
       - IP addresses
```

## How it works
This playbook utilises the [Napalm-Ansible](https://napalm.readthedocs.io/en/latest/tutorials/ansible-napalm.html) modules to extract facts from network devices and populate Netbox using the [Netbox Ansible modules](https://github.com/netbox-community/ansible_modules).

## Author
Sudarshan Vijaya Kumar

## TODO
- [ ] Add task to populate Address Prefixes
- [ ] Add task to populate VLANs
